try:
    import Tkinter as tk
except:
    import tkinter as tk


class Test():
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("DOCUMENT ANALYSER")
        self.root.geometry('500x300')
        redbutton = tk.Button(self.root, text="Welcome to NEW HORIZON", fg="blue", bg="white", font=("Arial", 16))
        redbutton.pack()
        button = tk.Button(self.root,
                           text='CLICK AND QUIT',fg="red",
                           command=self.quit)
        button.pack()
        self.root.mainloop()
    #print('hello world')

    def quit(self):
        self.root.destroy()

    #print('hello world')
app = Test()

print('hello world')


import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import names


def word_feats(words):
    return dict([(word, True) for word in words])

positive_vocab = ['awesome', 'liked', 'outstanding', 'fantastic', 'terrific', 'good', 'nice', 'great', ':)']
neutral_vocab = ['book', 'the', 'sound', 'i', 'it', 'was', 'is', 'actors', 'did', 'know', 'words','..','...']
negative_vocab = ['bad', 'terrible', 'useless', 'hate', 'worst']

positive_features = [(word_feats(pos), 'pos') for pos in positive_vocab]
negative_features = [(word_feats(neg), 'neg') for neg in negative_vocab]
neutral_features = [(word_feats(neu), 'neu') for neu in neutral_vocab]

# Our training set is then the sum of these three feature sets:

train_set = negative_features + positive_features + neutral_features

# We train the classifier:

classifier = NaiveBayesClassifier.train(train_set)

# Predict
neg = 0
pos = 0
neu = 0

sentence = "Terrible book, bad, worst, hate .."
sentence = sentence.lower()
print(sentence)
words = sentence.split(' ')
print(words)

for word in words:
    classResult = classifier.classify(word_feats(word))
    print(classResult)
    if classResult == 'pos':
        pos = pos + 1
    if classResult == 'neg':
        neg = neg + 1
    if classResult == 'neu':
        neu = neu + 1

print('No. of negative words:', neg)
print('no. of positive words:', pos)
print('no. of neutral words:', neu)

print('Positive: ' + str(float(pos) / len(words)))
print('Negative: ' + str(float(neg) / len(words)))

